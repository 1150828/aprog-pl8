/*
 * PL8 EX 3
 */
package aprog.pl8;

import java.util.Scanner;

public class Ex3 {
    
    public static Scanner in = new Scanner(System.in);
    
    private static void lerMatriz(int[][] matriz) {
        for(int i=0; i < matriz.length; i++) {
            for (int j=0; j < matriz[i].length; j++){
                System.out.print("Valor para posição ["+i+"]["+j+"]: ");
                matriz[i][j]=in.nextInt();
            }
        }
    }
    
    public static void mostrarMatriz(int[][] m){
        String matriz = "";
        System.out.println("\nMatriz: ");
        for(int i=0; i < m.length; i++) {
            for (int j=0; j < m[i].length; j++){
                matriz+=m[i][j] + " ";
            }
            matriz+="\n";
        }
        System.out.println(matriz);
    }

    public static boolean quadradoMagico(int[][] matriz) {
        int somaUm = 0, somaTemp = 0;
        for (int i = 0; i < matriz[0].length; i++) {
            somaUm+=matriz[0][i];
        }
        
        //Soma das Linhas
        for (int i = 0; i < matriz.length; i++) {
            somaTemp = 0;
            for (int j = 0; j < matriz.length; j++) {
                somaTemp+=matriz[i][j];
            }
            if(somaUm!=somaTemp){
                return false;
            }
        }
        
        //Soma das Colunas
        for (int i=0; i < matriz[0].length; i++){
            somaTemp = 0;
            for(int j=0; j < matriz.length; j++) {
                somaTemp+=matriz[j][i];
            }
            if(somaUm!=somaTemp){
                return false;
            }
        }
        
        //Soma Diagonal 1
        somaTemp = 0;
        for (int i = 0; i < matriz.length; i++) {
            somaTemp+=matriz[i][i];
        }
        if (somaTemp!=somaUm) {
            return false;
        }
        
        //Soma Diagonal 2
        somaTemp = 0;
        for (int i = 0; i < matriz.length; i++) {
            somaTemp+=matriz[matriz.length-1-i][i];
        }
        if (somaTemp!=somaUm) {
            return false;
        }
        
        return true;
    }
        
    public static void main(String[] args) {
        int n;
        do {
            System.out.print("Introduza o número de colunas e linhas que deseja na matriz: ");
            n=in.nextInt();
        } while (n<=0);
        int matriz[][] = new int[n][n];
        lerMatriz(matriz);
        mostrarMatriz(matriz);
        if (quadradoMagico(matriz)) {
            System.out.println("\nA matriz é um quadrado mágico.");
        } else {
            System.out.println("\nA matriz não é um quadrado mágico.");
        }
    }
    
}
