/*
 * PL8 EX 2
 * Funcionalidade: Este algoritmo cria uma matriz de dupla entrada e verifica o maior algarismo de cada uma das suas linhas
 */
package aprog.pl8;

public class Ex2 {
    
    public static void mostrarM(int[][] m){
        String matriz = "";
        System.out.println("\nMatriz: ");
        for(int i=0; i < m.length; i++) {
            for (int j=0; j < m[i].length; j++){
                matriz+=m[i][j] + " ";
            }
            matriz+="\n";
        }
        System.out.println(matriz);
    }
    
    public static void mediaM(int[][] m){
        int somaTotal=0;
        int[] somaColuna = new int[m[0].length];
        for (int i=0; i < m[0].length; i++){
            for(int j=0; j < m.length; j++) {
                somaColuna[i]+=m[j][i];
                somaTotal+=m[j][i];
            }
        }
        for (int i = 0; i < somaColuna.length; i++) {
            System.out.println("A média da coluna " + i + " é " + (float) somaColuna[i]/m.length);
        }
        System.out.println("A média global da matriz M é " + (float) somaTotal/(m.length*m[0].length));
    }
    
    public static void transpostaM(int[][] m){
        String matriz = "";
        System.out.println("\nMatriz Transposta: ");
        for (int i=0; i < m[0].length; i++){
            for(int j=0; j < m.length; j++) {
                matriz+=m[j][i] + " ";
            }
            matriz+="\n";
        }
        System.out.println(matriz);
    }
    
    public static void main(String[] args) {
        int x;
        int m[][] = {{1,4,2,1},{9,7,2,2},{1,7,3,5},{2,5,0,3},{4,7,2,1}};
        
        System.out.println("Maiores números de cada linha: ");
        for(int i=0; i < m.length; i++) {
            x = m[i][0];
            for (int j=1; j < m[i].length; j++){
                if (m[i][j] > x){
                    x= m[i][j];
                }
            }
            System.out.print(x + " ");
        }
        System.out.println("");
        
        mostrarM(m);
        mediaM(m);
        transpostaM(m);
    }
    
}
