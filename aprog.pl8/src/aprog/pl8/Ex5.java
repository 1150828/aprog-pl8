/*
 * PL8 EX 5
 */
package aprog.pl8;

import java.util.Scanner;

public class Ex5 {
    
    public static Scanner in = new Scanner(System.in);
    
    public static void lerMatriz(int[][] matriz) {
        for(int i=0; i < matriz.length; i++) {
            for (int j=0; j < matriz[i].length; j++){
                System.out.print("Valor para posição ["+i+"]["+j+"]: ");
                matriz[i][j]=in.nextInt();
            }
        }
    }
    public static void mostrarMatriz(int[][] m){
        String matriz = "";
        System.out.println("\nMatriz: ");
        for(int i=0; i < m.length; i++) {
            for (int j=0; j < m[i].length; j++){
                matriz+=m[i][j] + " ";
            }
            matriz+="\n";
        }
        System.out.println(matriz);
    }

    public static void somarElem(int[][] matriz, int[][] soma) {
        int somaTotal = 0, somaIJ, maiorSoma=0, mnElem=0;
        int[][] maioresNums = new int [matriz.length*matriz[0].length][2];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                somaTotal+=matriz[i][j];
            }
        }
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                somaIJ = somaTotal;
                
                for (int k = 0; k < matriz.length; k++) {
                    for (int l = 0; l < matriz[0].length; l++) {
                        if(k == i || l == j){
                            somaIJ -= matriz[k][l];
                        }
                    }
                }
                
                soma[i][j]=somaIJ;
            }
        }
        for (int i = 0; i < soma.length; i++) {
            for (int j = 0; j < soma[0].length; j++) {
                if(soma[i][j]>maiorSoma){
                    maiorSoma=soma[i][j];
                    maioresNums[0][0]=i;
                    maioresNums[0][1]=j;
                    mnElem=1;
                } else if(soma[i][j]==maiorSoma){
                   maioresNums[mnElem][0]=i;
                   maioresNums[mnElem][1]=j;
                   mnElem++;
                }
            }
        }
        if (mnElem==1) {
            System.out.println("Maior elemento está na posição:");
        } else {
            System.out.println("Maiores elementos estão nas posições:");
        }
        for (int i = 0; i < mnElem; i++) {
            System.out.println("["+maioresNums[i][0]+"]["+maioresNums[i][1]+"]");
        }
    }
    
    public static void main(String[] args) {
        int linhas, colunas;
        
        do {
            System.out.print("Introduza o número de linhas da matriz: ");
            linhas = in.nextInt();
        } while (linhas<=0);
        
        do {
            System.out.print("Introduza o número de colunas da matriz: ");
            colunas = in.nextInt();
        } while (colunas<=0);
        
        int[][] matriz = new int[linhas][colunas];
        int[][] soma = new int[linhas][colunas];
        
        lerMatriz(matriz);
        
        mostrarMatriz(matriz);
        
        somarElem(matriz, soma);
        
        mostrarMatriz(soma);
    }
    
}
