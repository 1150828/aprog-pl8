/*
 * PL8 EX 1
 */
package aprog.pl8;

import java.util.Scanner;

public class Ex1 {
    public static Scanner in = new Scanner(System.in);
    
    public static void main(String[] args) {
        int n,m;
        do{
            System.out.print("Números a introduzir na lista: ");
            n=in.nextInt();
        }while(n<=0);
        int[] lista = new int[n];
        lerNumeros(lista, n);
        do{
            System.out.print("Deseja visualizar quantos dos maiores números: ");
            m=in.nextInt();
        }while(m<=0 || m>n);
        maioresNumeros(lista, m);
    }

    public static void lerNumeros(int[] lista, int n) {
        int num;
        for (int i = 0; i < n; i++) {
            System.out.print("Número para a posição " + i + ": ");
            num=in.nextInt();
            lista[i]=num;
        }
    }
    public static void maioresNumeros(int[] lista, int m){
        int temp,fim;
        boolean trocas = false;
        fim = lista.length-1;
        do {
            trocas=false;
            for (int i=0; i<fim; i++){
                if(lista[i] < lista[i+1]) {
                    temp=lista[i]; 
                    lista[i]=lista[i+1]; 
                    lista[i+1]=temp; 
                    trocas=true;
                }
            }
            fim--;
        }while(trocas);
        System.out.print("\nMaiores " + m + " números introduzidos: ");
        for (int i = 0; i < m; i++) {
            System.out.print(lista[i] + " ");
        }
        System.out.println("");
    }

}
