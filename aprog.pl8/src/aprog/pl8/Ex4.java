/*
 * PL8 EX 4
 */
package aprog.pl8;

import java.util.Arrays;
import java.util.Scanner;

public class Ex4 {
    
    public static Scanner in = new Scanner(System.in);
    
    public static void lerMatriz(int[][] matriz) {
        for(int i=0; i < matriz.length; i++) {
            for (int j=0; j < matriz[i].length; j++){
                System.out.print("Valor para posição ["+i+"]["+j+"]: ");
                matriz[i][j]=in.nextInt();
            }
        }
    }
    public static void mostrarMatriz(int[][] m){
        String matriz = "";
        System.out.println("\nMatriz: ");
        for(int i=0; i < m.length; i++) {
            for (int j=0; j < m[i].length; j++){
                matriz+=m[i][j] + " ";
            }
            matriz+="\n";
        }
        System.out.println(matriz);
    }

    public static void repeticoesMatriz(int[][] matriz, int[][] repeticoes) {
        int elemRep = 0;
        boolean existe = false;
        for(int i=0; i < matriz.length; i++) {
            for (int j=0; j < matriz[0].length; j++){
                existe = false;
                for (int k = 0; k < elemRep; k++) {
                    if(repeticoes[0][k]==matriz[i][j]){
                        repeticoes[1][k]++;
                        existe = true;
                    }
                }
                if(!existe){
                    repeticoes[0][elemRep]=matriz[i][j];
                    repeticoes[1][elemRep]=1;
                    elemRep++;
                }
            }
        }
        int temp0,temp1,fim;
        boolean trocas;
        fim = elemRep;
        do {
            trocas=false;
            for (int i=0; i<fim; i++){
                if(repeticoes[1][i] < repeticoes[1][i+1]) {
                    temp0=repeticoes[0][i];
                    temp1=repeticoes[1][i];
                    repeticoes[0][i]=repeticoes[0][i+1];
                    repeticoes[1][i]=repeticoes[1][i+1];
                    repeticoes[0][i+1]=temp0;
                    repeticoes[1][i+1]=temp1;
                    trocas=true;
                }
            }
            fim--;
        }while(trocas);
        for (int i = 0; i < elemRep; i++) {
            System.out.print(repeticoes[0][i] + ":" + repeticoes[1][i] + " ");
        }
        System.out.println("");
    }
    
    public static void main(String[] args) {
        int linhas, colunas;
        
        do {
            System.out.print("Introduza o número de linhas da matriz: ");
            linhas = in.nextInt();
        } while (linhas<=0);
        
        do {
            System.out.print("Introduza o número de colunas da matriz: ");
            colunas = in.nextInt();
        } while (colunas<=0);
        
        int[][] matriz = new int[linhas][colunas];
        int[][] repeticoes = new int[2][linhas*colunas];
        
        lerMatriz(matriz);
        
        mostrarMatriz(matriz);
        
        repeticoesMatriz(matriz, repeticoes);
    }
    
}
